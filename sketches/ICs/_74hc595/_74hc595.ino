int latchPin = 13;
int clockPin = 12;
int dataPin = 11;

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  Serial.begin(9600);
}


long data  = 0;
int bitOn = 1;
byte getByte(byteIndex){
   rowOutputBuffer[ibyte] &= ~(1 << ibit);
  else
    rowOutputBuffer[ibyte] |= 1 << ibit;
}
void loop() {
    digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST,  0b01010101);
  digitalWrite(latchPin, HIGH);
  return;
  for (int numberToDisplay = 0; numberToDisplay < 8; numberToDisplay++) {
    //  for (int numberToDisplay = 0; numberToDisplay < 256; numberToDisplay++) {
    // take the latchPin low so
    // the LEDs don't change while you're sending in bits:
    digitalWrite(latchPin, LOW);
    // shift out the bits:
    int sr_value  = (1<<numberToDisplay)>>1;
    Serial.println(String(numberToDisplay) + ":" + String(sr_value));
    shiftOut(dataPin, clockPin, MSBFIRST,  sr_value);
    //take the latch pin high so the LEDs will light up:
    digitalWrite(latchPin, HIGH);
    // pause before next value:
    delay(500);
  }
}
