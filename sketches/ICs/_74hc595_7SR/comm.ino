char inputString[100];
byte inputPos = 0;

void setupComm() {
  Serial.begin(9600);
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      writeStringColorSequence(inputString, inputPos);
      inputPos = 0;
    } else {
      inputString[inputPos] = inChar;
      inputPos++;
    }
  }
}

