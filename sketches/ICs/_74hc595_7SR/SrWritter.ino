const int latchPin = 7;
const int clockPin = 6;
const int dataPin = 5;

void setupSrs() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
/*
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST,  128);
  digitalWrite(latchPin, HIGH);
  delay(1000);
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST,  2);
  digitalWrite(latchPin, HIGH);
  */
}

void writeToSrs(byte* outputbuffer) {
  digitalWrite(latchPin, LOW);
  for (int i = SrsCount-1; i >=0 ; i--) {
    Serial.println(outputbuffer[i]);
    shiftOut(dataPin, clockPin, MSBFIRST,  outputbuffer[i]);
  }
  digitalWrite(latchPin, HIGH);
  /*
  for (int numberToDisplay = 0; numberToDisplay < 8; numberToDisplay++) {
    //  for (int numberToDisplay = 0; numberToDisplay < 256; numberToDisplay++) {
    // take the latchPin low so
    // the LEDs don't change while you're sending in bits:
    digitalWrite(latchPin, LOW);
    // shift out the bits:
    int sr_value  = (1 << numberToDisplay) >> 1;
    Serial.println(String(numberToDisplay) + ":" + String(sr_value));
    shiftOut(dataPin, clockPin, MSBFIRST,  sr_value);
    //take the latch pin high so the LEDs will light up:
    digitalWrite(latchPin, HIGH);
    // pause before next value:
  }
  */
}


