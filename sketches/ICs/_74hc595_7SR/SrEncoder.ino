const int SrsCount = 7;
byte ledsMapping[] = {
   1,  2,  3,  4,  5,  6,  7,
   9, 10, 11, 12, 13, 14, 15,
  17, 18, 19, 20, 21, 22, 23,
  25, 26, 27, 28, 29, 30, 31,
  33, 34, 35, 36, 37, 38, 39,
  41, 42, 43, 44, 45, 46, 47,
  49, 50, 51, 52, 53, 54, 55
};
byte srOutputBuffer[SrsCount];
boolean rgb[3];

void setOutputBit(byte bitNb) {
//  Serial.println("set bitNb" + String(bitNb));
  byte dest = ledsMapping[bitNb];
//  Serial.println("dest" + String(dest));
  byte ibyte = dest / 8;
//  Serial.println("ibyte" + String(ibyte));
  byte ibit = dest % 8;
//  Serial.println("ibit" + String(ibit));
//  Serial.println("srOutputBuffer[ibyte]" + String(srOutputBuffer[ibyte]));
  srOutputBuffer[ibyte] |= 1 << ibit;
//  Serial.println("srOutputBuffer[ibyte]" + String(srOutputBuffer[ibyte]));
}


void fillInOutputBuffer(char* strColorSequence, byte charCount) {
  for (int i = 0; i < SrsCount; i++) {
    srOutputBuffer[i] = 0;
  }

  for (int i = 0; i < charCount; i++) {
    char ledColor = strColorSequence[i];
    rgb[0] = false;
    rgb[1] = false;
    rgb[2] = false;

    switch (ledColor) {
      case 'r':
        rgb[0] = true;
        break;
      case 'g':
        rgb[1] = true;
        break;
      case 'b':
        rgb[2] = true;
        break;
      case 'o':
        rgb[0] = true;
        rgb[1] = true;
        break;
      case 'p':
        rgb[0] = true;
        rgb[2] = true;
        break;
      case 'c':
        rgb[1] = true;
        rgb[2] = true;
        break;
      case 'a':
        rgb[0] = true;
        rgb[1] = true;
        rgb[2] = true;
        break;
      default:
        break;
    }

    byte ledOffset = i * 3;
    for (int j = 0; j < 3; j++)
      if (rgb[j])
        setOutputBit(ledOffset + j);
  }
}
void writeStringColorSequence(char* strColorSequence, byte charCount) {
  fillInOutputBuffer(strColorSequence, charCount);
  writeToSrs(srOutputBuffer);
}

