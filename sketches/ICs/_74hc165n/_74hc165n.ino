/*
IC  pins:
 1-> parallel load
 2-> clock  
 8-> gnd
 9-> data
 10-> gnd (DS)
 15-> gnd
 16->vcc
 
 Parallel in into to IC pin:
 0-> 11
 1-> 12
 2-> 13 
 3-> 14
 4-> 3
 5-> 4
 6-> 5
 7-> 6
 */
#include <_74hc165.h>

byte clockPin        = 3; 
byte ploadPin        = 2;  
byte dataPin         = 4; 

byte dataIn2check[] = {0, 1, 2, 3, 4, 5, 6, 7};
byte pinValues;
byte oldPinValues;

_74hc165 _74hc165a   = _74hc165(dataPin, clockPin, ploadPin);

byte dataIn2checkMask = 0;
void setup()
{
  Serial.begin(9600);

  for(byte i = 0;i<sizeof(dataIn2check);i++)
    dataIn2checkMask |= 1<<dataIn2check[i];
  pinValues = _74hc165a.readByte();
  display_pin_values();
  oldPinValues = pinValues;
}

void loop()
{
  pinValues = _74hc165a.readByte();

  pinValues &= dataIn2checkMask;
  /* If there was a chage in state, display which ones changed.
   */
  if(pinValues != oldPinValues)
  {
    Serial.print("*Pin value change detected*\r\n");
    display_pin_values();
    oldPinValues = pinValues;
  }
  delay(50);
}

void display_pin_values()
{
  Serial.print("Pin States:\r\n");

  for(int i = 0; i < 8; i++)
  {
    Serial.print("  Pin-");
    Serial.print(i);
    Serial.print(": ");

    if (dataIn2checkMask & 1<<i ){
      if((pinValues >> i) & 1)
        Serial.print("HIGH");
      else
        Serial.print("LOW");
    }
    else
      Serial.print("N/A");

    Serial.print("\r\n");
  }

  Serial.print("\r\n");
}






