const int SrsCount = 3;
const int Rows = 2;
byte rowsMapping[Rows] = {
  16, 23
};

byte colorLedsMapping[] = {
  1,  2,  3,  4,  5,  6,
  7,  9, 10, 11, 12, 13
};

byte resultLedsMapping[] = {
  14, 15, 17, 18,
  19, 20, 21, 22
};

byte srOutputBuffer[Rows][SrsCount];

void setupEncoder() {
  for (byte i = 0; i < Rows; i++) {
    for (byte j = 0; j < SrsCount; j++) {
      srOutputBuffer[i][j] = 0;
    }
    setOutputBit(i, rowsMapping[i], true);
  }

  writeToSrs(srOutputBuffer[0]);
}

void setOutputBit(byte row, byte bitNb, bool isOn) {
  byte ibyte = bitNb / 8;

  byte ibit = bitNb % 8;
  if (isOn)
    srOutputBuffer[row][ibyte] |= 1 << ibit;
  else
    srOutputBuffer[row][ibyte] &= ~(1 << ibit);
}

void setRowPositionColor(byte row, byte pos, byte color) {
  byte redPosition = pos * 3 ;
  for (byte i = 0; i < 3; i++) {
    setOutputBit(row, colorLedsMapping[redPosition + i], (color & (1 <<  i)));
  }
}

void setRowResultPositionColor(byte row, byte pos, byte color) {
  byte redPosition = pos * 2 ;
  for (byte i = 0; i < 2; i++) {
    setOutputBit(row, resultLedsMapping[redPosition + i], (color & (1 << i)));
  }
}

byte row = 0;
byte col = 0;
void writeColor(byte color) {
  if (col < 4) { //rgb
    setRowPositionColor(row, col, color);
  } else { //rb
    setRowResultPositionColor(row, col - 4, color);
  }
  col++;
  if (col == 8) {
    col = 0;
    row = (row + 1) % 2;
  }
}

byte row2showNext = 0;
byte rows2show = 10;
byte emptyRow[] = {0, 0, 0};
int  showTimeEachRows = 1000;
int rowsShowSinceLastTime = 0;
unsigned long lastShowTime = millis();
void displayNextRow() {

  if (row2showNext < 2)
    writeToSrs(srOutputBuffer[row2showNext]);
  else
    writeToSrs(emptyRow);

  row2showNext++;
  if (row2showNext == rows2show) {
    row2showNext = 0;
    rowsShowSinceLastTime++;
    if (rowsShowSinceLastTime == showTimeEachRows) {
      rowsShowSinceLastTime = 0;
      Serial.println("Time to print " + String(showTimeEachRows) + " rows: " + String(millis() - lastShowTime));
      lastShowTime = millis();
    }
  }
}
