#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

const int dataPin = 8;
const int latchPin = 9;
const int clockPin = 10;

void setupSrs() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  /*
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST,  128);
    digitalWrite(latchPin, HIGH);
    delay(1000);
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST,  2);
    digitalWrite(latchPin, HIGH);
    */
}


void writeToSrs(byte* outputbuffer) {

  //digitalWrite(latchPin, LOW);
  //PORTB &= ~(1 << 1);
  cbi(PORTB, 1);
  /*
    for (int i = SrsCount - 1; i >= 0 ; i--)
      shiftOut(dataPin, clockPin, MSBFIRST,  outputbuffer[i]);
      */
  for (int i = SrsCount - 1; i >= 0 ; i--) {
    byte byte2output = outputbuffer[i];
    for  (int j = 7; j >= 0; j--) {
      //PORTB &= ~(1 << 2);
      cbi(PORTB, 2);
      if (byte2output & (1 << j))
        //PORTB |= 1 ;
        sbi(PORTB, 0);
      else
        //        PORTB &= ~101 ;
        cbi(PORTB, 0);

      //PORTB |= 1 << 2 ;
      sbi(PORTB, 2);
    }
  }


  //digitalWrite(latchPin, HIGH);
  PORTB |= 1 << 1;
}


