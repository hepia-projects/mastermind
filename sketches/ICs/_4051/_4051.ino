#define z 8
const byte selectors[] = {9, 10, 11}; //s0, s1, s2

void setup() {
  Serial.begin(9600);
  pinMode(z, INPUT_PULLUP);
  for (byte i = 0; i < 3; i++)
    pinMode(selectors[i], OUTPUT);
}

void loop() {
  for (byte i = 0; i < 8; i++) {
    for (byte j = 0; j < 3; j++) {
      digitalWrite(selectors[j], i & (1 << j));
    }
    byte input = digitalRead(z);
    if (input == 0)
      Serial.println(String(i));
  }
  delay(50);
}
