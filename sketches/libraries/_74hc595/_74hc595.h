#ifndef _74hc595_h
#define _74hc595_h

#include "Arduino.h"

class _74hc595
{
public:
  _74hc595(byte dataPin, byte clockPin, byte latchPin, byte shiftRegistersCount);
  void writeByte(byte byte2write);
  void writeBytes(byte* bytes2write);
private:
  byte _dataPin, _clockPin, _latchPin, _shiftRegistersCount;
};

#endif


