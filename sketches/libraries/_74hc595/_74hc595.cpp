#include <Arduino.h>
#include <_74hc595.h>


_74hc595::_74hc595(byte dataPin, byte clockPin, byte latchPin, byte shiftRegistersCount){
	pinMode(latchPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(dataPin, OUTPUT);

	_shiftRegistersCount = shiftRegistersCount;
	_latchPin = latchPin;
	_dataPin = dataPin;
	_clockPin = clockPin;
}

void _74hc595::writeByte(byte byte2write){
	if (_shiftRegistersCount > 1)
		return;
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST,  byte2write);
	digitalWrite(_latchPin, HIGH);
}
void _74hc595::writeBytes(byte* bytes2write) {
	digitalWrite(_latchPin, LOW);
	for (int i = _shiftRegistersCount-1; i >=0 ; i--) {
		//Serial.println(outputbuffer[i]);
		shiftOut(_dataPin, _clockPin, MSBFIRST,  bytes2write[i]);
	}
	digitalWrite(_latchPin, HIGH);
}



