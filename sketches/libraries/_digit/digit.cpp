#include <Arduino.h>
#include <digit.h>

byte pins[7];
const byte numbers[] = {119, 65, 127-4-64, 107, 77, 110, 126, 127-4-8-16-32, 127, 127-16, 0};
bool _isEnabledLow;

digit::digit(byte pin2, byte pin4, byte pin5, byte pin6, byte pin7, byte pin9, byte pin10, bool isEnabledLow){
	pins[0] = pin2;
	pins[1] = pin4;
	pins[2] = pin5;
	pins[3] = pin6;
	pins[4] = pin7;
	pins[5] = pin9;
	pins[6] = pin10;
	
	_isEnabledLow = isEnabledLow;
	
  for (byte i = 0; i < 7; i++) {
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], isEnabledLow);
  }
}

void digit::clear(){
	digit:display(10);
}
void digit::display(byte nb) {
  byte nbByte = numbers[nb];
  for (byte i = 0; i < 7; i++) {
    digitalWrite(pins[6 - i], (nbByte & (1 << i)) != 0 ? !_isEnabledLow : _isEnabledLow);
  }
}



