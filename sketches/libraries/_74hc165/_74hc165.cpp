#include <Arduino.h>
#include <_74hc165.h>

byte _dataPin, _clockPin, _ploadPin, _clockEnabledPin, _shiftRegisters, _bitOrder;
 
_74hc165::_74hc165(byte dataPin, byte clockPin, byte ploadPin, byte shiftRegisters, byte clockEnabledPin, byte bitOrder){
  pinMode(ploadPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(clockEnabledPin, OUTPUT);
  pinMode(dataPin, INPUT);

  digitalWrite(clockPin, LOW);
  digitalWrite(ploadPin, HIGH);
  
  _bitOrder = bitOrder;
  _shiftRegisters = shiftRegisters;
  _ploadPin = ploadPin;
  _clockEnabledPin = clockEnabledPin;
  _dataPin = dataPin;
  _clockPin = clockPin;
}



byte _74hc165::readByte(){ 
	return readBytes()[0] ;
}

byte* _74hc165::readBytes(){      
	
    digitalWrite(_clockEnabledPin, HIGH);
    digitalWrite(_ploadPin, LOW);
    //delayMicroseconds(10);
    digitalWrite(_ploadPin, HIGH);
    digitalWrite(_clockEnabledPin, LOW);
  
	byte res[_shiftRegisters];
	byte bitVal;
	for(byte j = 0; j < _shiftRegisters; j++)
    {
		byte byteRes = 0;
		for(byte i = 0; i < 8; i++)
		{
			bitVal = digitalRead(_dataPin);

			if (_bitOrder == LSBFIRST)
				byteRes |= bitVal << i;
			else
				byteRes |= bitVal << (7 - i);
		
			digitalWrite(_clockPin, HIGH);
			//delayMicroseconds(5);
			digitalWrite(_clockPin, LOW);
		}
		res[j] = byteRes;
	}
    return res;
}


