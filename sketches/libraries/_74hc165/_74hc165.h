#ifndef _74hc165_h
#define _74hc165_h

#include "Arduino.h"

class _74hc165
{
  public:
	_74hc165(byte dataPin, byte clockPin, byte ploadPin, byte clockEnabledPin, byte shiftRegisters=1, byte bitOrder=MSBFIRST);
	byte readByte();
	byte* readBytes();
  
};

#endif

